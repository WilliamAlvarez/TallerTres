﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerTres.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace TallerTres
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class CrearUsuarios : ContentPage
	{
		public CrearUsuarios ()
		{
			InitializeComponent ();
		}
        public void ButtonClick(object sender, EventArgs e)
        {

            //   crear objeto del modelo tarea
            Usuario usuario = new Usuario()
            {
                NameUser = nameUser.Text,
                Password = password.Text,
                Avatar = avatar.Text,
                State = false

            };

            // conexion a la base de datos
            using (SQLite.SQLiteConnection connection = new SQLite.SQLiteConnection(App.urlBd))
            {
                // crear tabla en base de datos
                connection.CreateTable<Usuario>();

                // crear registro en la tabla
                var result = connection.Insert(usuario);

                if (result > 0)
                {
                    DisplayAlert("Correcto", "La tarea se creo correctamente", "OK");
                }
                else
                {
                    DisplayAlert("Incorrecto", "La tarea no fue creada", "OK");
                }
            }
        }


        async public void LisUsers(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new ListarUsuarios());
        }
    }
}