﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TallerTres.Models;
using Xamarin.Forms;

namespace TallerTres
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
		}

        async private void ButtonClickedPersonas(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearPersonas());
        }

        async private void ButtonClickedUsuarios(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CrearUsuarios());
        }



    }
}
